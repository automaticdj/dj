# This file is copyrighted.
# See the README.md file at the top-level directory of this distribution and at https://bitbucket.org/automaticdj/dj/src/master/README.md .

'''
Feature extraction used in the machine learning cross fade detection
Used in production as in the feature extraction of the annotated cross fades.
'''

import numpy as np

from timestretching import time_stretch_and_pitch_shift

from essentia.standard import MonoLoader
from essentia.standard import FrameGenerator

# Feature: energy in melbands
from essentia.standard import Spectrum, Windowing
from essentia.standard import MelBands

from essentia import Pool
from essentia.standard import OnsetDetection, FrameGenerator

from essentia.standard import Loudness

# Constants

SAMPLE_RATE = 44100
FRAME_SIZE_FFT = 1024
HOP_SIZE_FFT = 512

TEMPO = 175

## Feature extraction
ODF_METHODS = ['hfc', 'complex']  # NOTE: complex = CSD

## Feature extraction modes
ONE_VALUE_DIFFERENCE = 'one_value_difference'
RUNNING_DIFFERENCE = 'running_difference'
DEFAULT_FE_MODE = RUNNING_DIFFERENCE # Used in live execution of DJ software tool.

DEFAULT_EXTENDED_LENGTH_SEGMENTS = True

## Feature construction
SEGMENT_SIZE_IN_BEATS = 8
EXTENDED_SEGMENT_SIZE_IN_BEATS = 4 # the length of the extended part of the segment piece

# Auxiliary methods

def extract_beat_audio(beat_idx, t_audio_stretched, t_beats_str):
    beat_begin_in_samples = int(t_beats_str[beat_idx] * SAMPLE_RATE)
    beat_end_in_samples = int(t_beats_str[beat_idx + 1] * SAMPLE_RATE)
    if (beat_end_in_samples - beat_begin_in_samples) % 2 == 1:  # Essentia needs even size
        # arrays to compute FFT.
        beat_end_in_samples -= 1
    beat_audio = t_audio_stretched[beat_begin_in_samples: beat_end_in_samples]
    return beat_audio


# Extract and save basic features that are needed (used among others in the annotation phase of the DJ software).

def extract_basic_feature_fft(t_beats_str, t_audio_stretched):
    w = Windowing(type='hann')
    fft = np.fft.fft
    pool = Pool()

    if t_audio_stretched.dtype != 'float32':
        t_audio_stretched = t_audio_stretched.astype('float32')
    for frame in FrameGenerator(t_audio_stretched, frameSize=FRAME_SIZE_FFT, hopSize=HOP_SIZE_FFT):
        pool.add('audio.windowed_frames', w(frame))

    fft_t_audio_stretched = fft(pool['audio.windowed_frames']).astype('complex64')
    fft_mag_t_audio_stretched = np.absolute(fft_t_audio_stretched)
    fft_phase_t_audio_stretched = np.angle(fft_t_audio_stretched)


    return {
        'mag': fft_mag_t_audio_stretched,
        'phase': fft_phase_t_audio_stretched
    }

'''
Extract MFCC for each beat.
Note: the features of the audio before the first beat (beat idx = 0) and the last beat is not extracted.
'''
def extract_basic_feature_mfcc(t_beats_str, t_audio_stretched):
    # Initialise algorithm
    number_bins = 12

    w = Windowing(type='hann')
    spectrum = Spectrum()  # FFT would return complex FFT, we only want magnitude
    melbands = MelBands(numberBands=number_bins)

    if t_audio_stretched.dtype != 'float32':
        t_audio_stretched = t_audio_stretched.astype('float32')
    def extract_mfcc(frame):
        return melbands(spectrum(w(frame)))

    # Extract MFCC for each beat
    beat_mfccs = []
    for beat_idx, beat in enumerate(t_beats_str[:-1]):
        beat_audio = extract_beat_audio(beat_idx, t_audio_stretched, t_beats_str)
        beat_mfcc = extract_mfcc(beat_audio)
        beat_mfccs.append(beat_mfcc)

    return beat_mfccs

'''
Extract loudness for each beat.
Note: the features of the audio before the first beat (beat idx = 0) and the last beat is not extracted.
'''
def extract_basic_feature_loudness(t_beats_str, t_audio_stretched):
    # Initialise the algorithm.
    w = Windowing(type='hann')
    loudness = Loudness()

    if t_audio_stretched.dtype != 'float32':
        t_audio_stretched = t_audio_stretched.astype('float32')
    def extract_loudness(frame):
        return loudness(w(frame))

    # Extract loudness for each beat
    beat_loudnesses = []
    for beat_idx, beat in enumerate(t_beats_str[:-1]):
        beat_audio = extract_beat_audio(beat_idx, t_audio_stretched, t_beats_str)
        beat_loudness = extract_loudness(beat_audio)
        beat_loudnesses.append(beat_loudness)

    return beat_loudnesses

def extract_basic_feature_odf_hwr(t_beats_str, t_audio_stretched,
                                  t_fft_mag_per_frame, t_fft_phase_per_frame):

    # Initialise the algorithm.
    Q = 16.0

    def extract_odf_hwr(t_fft_mag_per_frame, t_fft_phase_per_frame, odf_method):
        odf = OnsetDetection(method=odf_method)
        pool = Pool()

        for fft_mag_frame, fft_phase_frame in zip(t_fft_mag_per_frame, t_fft_phase_per_frame):
            pool.add('odf.per_frame_idx', odf(fft_mag_frame, fft_phase_frame))

        def adaptive_mean(x, N):
            return np.convolve(x, [1.0] * int(N), mode='same') / N

        gamma_mean_per_frame_idx = adaptive_mean(pool['odf.per_frame_idx'], Q)
        gamma_hwr_per_frame_idx = (pool['odf.per_frame_idx'] - gamma_mean_per_frame_idx).clip(min=0)

        return gamma_hwr_per_frame_idx

    # Extract ODF HWR for each frame for each ODF method
    t_gamma_hwr_per_frame_idx_dict = dict()
    for odf_method in ODF_METHODS:
        t_gamma_hwr_per_frame_idx_dict[odf_method] = extract_odf_hwr(t_fft_mag_per_frame, t_fft_phase_per_frame, odf_method)

    return t_gamma_hwr_per_frame_idx_dict


'''
Is ran among other in the annotation phase in the DJ software tool.
Note: all features are extracted from the stretsched audio.
Caveat: to make a change in the saved annotations, delete the existing annotations files first.
'''


def extract_all_basic_features(t_tempo, t_beats, t_audio, target_tempo=None, semitone_shift=None):
    if target_tempo is None:
        target_tempo = TEMPO
    if semitone_shift is None:
        semitone_shift = 0

    # Prepare input
    f_t = t_tempo / float(target_tempo)
    t_beats_str = f_t*np.array(t_beats)
    t_audio_stretched = time_stretch_and_pitch_shift(t_audio, f_t, semitone_shift)

    # Calculate all basic feature types
    all_basic_features = {
        'fft_per_frame': extract_basic_feature_fft(t_beats_str, t_audio_stretched),     # fft (per frame)
        'mfcc_per_beat': extract_basic_feature_mfcc(t_beats_str, t_audio_stretched),    # mfcc (per beat)
        'loudness_per_beat': extract_basic_feature_loudness(t_beats_str, t_audio_stretched),    # loudness (per beat)
    }
    ## Dependant on other basic features
    all_basic_features['odf_hwr_per_frame'] = extract_basic_feature_odf_hwr(
        t_beats_str, t_audio_stretched,
        all_basic_features['fft_per_frame']['mag'], all_basic_features['fft_per_frame']['phase']
    )  # odf hwr (3x types) (per frame)

    return all_basic_features


# Feature extractors (of a segment pair)
'''
Note: All input metrics are wrt T1
'''
def extract_feature_e_in_melbands_for_segment_pair(t1_first_beat_in_segment_in_beat_idx,
                                                   segment_pair_switching_point_in_beat_idx,
                                                   t2_last_beat_in_segment_in_beat_idx,
                                                   offset_B_wrt_A_in_beat_idx,
                                                   t1_mfcc_per_beat, t2_mfcc_per_beat,
                                                   mode=DEFAULT_FE_MODE,
                                                   extended_length_semgents=DEFAULT_EXTENDED_LENGTH_SEGMENTS):

    features_segment_pair = []

    # print('DEBUG: left segment of  the pair')
    # Track A, left segement
    ## Isolated features
    left_segment_isolated_beat_idx = segment_pair_switching_point_in_beat_idx - 1
    left_segment_isolated_beat_features = t1_mfcc_per_beat[left_segment_isolated_beat_idx]
    features_segment_pair.append(left_segment_isolated_beat_features)
    # print('DEBUG: added feature of  isolated beat of the segment -> beat idx {}'.format(left_segment_isolated_beat_idx))
    ## Contextual features
    if extended_length_semgents is False:
        contextual_beat_idx_start = t1_first_beat_in_segment_in_beat_idx
        contextual_beat_idx_stop = left_segment_isolated_beat_idx - 1 # (included)
    else:
        contextual_beat_idx_start = t1_first_beat_in_segment_in_beat_idx
        contextual_beat_idx_stop = (left_segment_isolated_beat_idx - 1) + EXTENDED_SEGMENT_SIZE_IN_BEATS # (included)
    for contextual_beat_idx in range(contextual_beat_idx_start, contextual_beat_idx_stop+1):
        if mode == ONE_VALUE_DIFFERENCE:
            contextual_beat_features = t1_mfcc_per_beat[contextual_beat_idx]
            contextual_beat_difference = contextual_beat_features - left_segment_isolated_beat_features
            features_segment_pair.append(contextual_beat_difference)
            # print('DEBUG: added feature of contextual beat -> beat idx {}'.format(contextual_beat_idx))
        elif mode == RUNNING_DIFFERENCE:
            contextual_beat_difference = t1_mfcc_per_beat[contextual_beat_idx+1] - t1_mfcc_per_beat[contextual_beat_idx]
            features_segment_pair.append(contextual_beat_difference)
        else:
            raise Exception('Mode invalid.')

    # print('DEBUG: right segment of  the pair')
    # print('DEBUG: t2_last_beat_in_segment_in_beat_idx = {} offset_B_wrt_A_in_beat_idx = {}, '.format(t2_last_beat_in_segment_in_beat_idx, offset_B_wrt_A_in_beat_idx))
    # Track B, right segment
    right_segment_isolated_beat_idx = segment_pair_switching_point_in_beat_idx - offset_B_wrt_A_in_beat_idx
    # print('DEBUG: right_segment_isolated_beat_idx = {} in T1 beat idx: {}'.format(right_segment_isolated_beat_idx, right_segment_isolated_beat_idx + offset_B_wrt_A_in_beat_idx))
    right_segment_isolated_beat_features = t2_mfcc_per_beat[right_segment_isolated_beat_idx]
    features_segment_pair.append(right_segment_isolated_beat_features)
    # print('DEBUG: added feature of  isolated beat of the segment -> beat idx {}'.format(right_segment_isolated_beat_idx))
    ## Contextual features
    if extended_length_semgents is False:
        contextual_beat_idx_start = right_segment_isolated_beat_idx + 1
        contextual_beat_idx_stop = t2_last_beat_in_segment_in_beat_idx - offset_B_wrt_A_in_beat_idx # (included)
    else:
        contextual_beat_idx_start = (right_segment_isolated_beat_idx + 1) - EXTENDED_SEGMENT_SIZE_IN_BEATS
        contextual_beat_idx_stop = t2_last_beat_in_segment_in_beat_idx - offset_B_wrt_A_in_beat_idx  # (included)
    for contextual_beat_idx in range(contextual_beat_idx_start, contextual_beat_idx_stop+1):
        if mode == ONE_VALUE_DIFFERENCE:
            # print('DEBUG: in T1 beat idx: contextual_beat_idx = {}'.format(contextual_beat_idx+offset_B_wrt_A_in_beat_idx))
            contextual_beat_features = t2_mfcc_per_beat[contextual_beat_idx]
            contextual_beat_difference = contextual_beat_features - right_segment_isolated_beat_features
            features_segment_pair.append(contextual_beat_difference)
            # print('DEBUG: added feature of contextual beat -> beat idx {}'.format(contextual_beat_idx))
        elif mode == RUNNING_DIFFERENCE:
            contextual_beat_difference = t2_mfcc_per_beat[contextual_beat_idx] - t2_mfcc_per_beat[contextual_beat_idx-1]
            features_segment_pair.append(contextual_beat_difference)
        else:
            raise Exception('Mode invalid.')

    return features_segment_pair

'''
Note: All input metrics are wrt T1
'''
def extract_feature_loudness_for_segment_pair(t1_first_beat_in_segment_in_beat_idx,
                                              segment_pair_switching_point_in_beat_idx,
                                              t2_last_beat_in_segment_in_beat_idx,
                                              offset_B_wrt_A_in_beat_idx,
                                              t1_loudness_per_beat, t2_loudness_per_beat,
                                              mode=DEFAULT_FE_MODE,
                                              extended_length_semgents=DEFAULT_EXTENDED_LENGTH_SEGMENTS):
    features_segment_pair = []

    # print('DEBUG: LEFT segment of  the segment-pair')
    # Track A, left segement
    ## Isolated features
    left_segment_isolated_beat_idx = segment_pair_switching_point_in_beat_idx - 1
    left_segment_isolated_beat_features = t1_loudness_per_beat[left_segment_isolated_beat_idx]
    features_segment_pair.append(left_segment_isolated_beat_features)
    # print('DEBUG: added feature (value {}) of  isolated beat of the segment (beat idx {})'
    #       .format(left_segment_isolated_beat_features, left_segment_isolated_beat_idx))
    ## Contextual features
    if extended_length_semgents is False:
        contextual_beat_idx_start = t1_first_beat_in_segment_in_beat_idx
        contextual_beat_idx_stop = left_segment_isolated_beat_idx - 1 # (included)
    else:
        contextual_beat_idx_start = t1_first_beat_in_segment_in_beat_idx
        contextual_beat_idx_stop = (left_segment_isolated_beat_idx - 1) + EXTENDED_SEGMENT_SIZE_IN_BEATS # (included)
    for contextual_beat_idx in range(contextual_beat_idx_start, contextual_beat_idx_stop+1):
        if mode == ONE_VALUE_DIFFERENCE:
            contextual_beat_features = t1_loudness_per_beat[contextual_beat_idx]
            contextual_beat_difference = contextual_beat_features - left_segment_isolated_beat_features
            features_segment_pair.append(contextual_beat_difference)
            # print('DEBUG: added feature (value {}) of  contextual beat (beat idx {})'
            #   .format(contextual_beat_difference, contextual_beat_idx))
        elif mode == RUNNING_DIFFERENCE:
            contextual_beat_difference = t1_loudness_per_beat[contextual_beat_idx+1] - t1_loudness_per_beat[contextual_beat_idx]
            features_segment_pair.append(contextual_beat_difference)
        else:
            raise Exception('Mode invalid.')

    # print('DEBUG: RIGHT segment of  the segment-pair')
    # Track B, right segment
    right_segment_isolated_beat_idx = segment_pair_switching_point_in_beat_idx - offset_B_wrt_A_in_beat_idx
    right_segment_isolated_beat_features = t2_loudness_per_beat[right_segment_isolated_beat_idx]
    features_segment_pair.append(right_segment_isolated_beat_features)
    # print('DEBUG: added feature of  isolated beat of the segment -> beat idx {}'.format(right_segment_isolated_beat_idx))
    # print('DEBUG: added feature (value {}) of  isolated beat of the segment (beat idx {})'
    #       .format(right_segment_isolated_beat_features, right_segment_isolated_beat_idx))
    ## Contextual features
    if extended_length_semgents is False:
        contextual_beat_idx_start = right_segment_isolated_beat_idx + 1
        contextual_beat_idx_stop = t2_last_beat_in_segment_in_beat_idx - offset_B_wrt_A_in_beat_idx # (included)
    else:
        contextual_beat_idx_start = (right_segment_isolated_beat_idx + 1) - EXTENDED_SEGMENT_SIZE_IN_BEATS
        contextual_beat_idx_stop = t2_last_beat_in_segment_in_beat_idx - offset_B_wrt_A_in_beat_idx  # (included)
    for contextual_beat_idx in range(contextual_beat_idx_start, contextual_beat_idx_stop+1):
        if mode == ONE_VALUE_DIFFERENCE:
            contextual_beat_features = t2_loudness_per_beat[contextual_beat_idx]
            contextual_beat_difference = contextual_beat_features - right_segment_isolated_beat_features
            features_segment_pair.append(contextual_beat_difference)
            # print('DEBUG: added feature (value {}) of  contextual beat (beat idx {})'
            #   .format(contextual_beat_difference, contextual_beat_idx))
        elif mode == RUNNING_DIFFERENCE:
            contextual_beat_difference = t2_loudness_per_beat[contextual_beat_idx] - t2_loudness_per_beat[contextual_beat_idx-1]
            features_segment_pair.append(contextual_beat_difference)
        else:
            raise Exception('Mode invalid.')

    return features_segment_pair

'''
Note: All input metrics are wrt T1
'''
def extract_feature_sum_onset_function_for_segment_pair(t1_first_beat_in_segment_in_beat_idx,
                                                        segment_pair_switching_point_in_beat_idx,
                                                        t2_last_beat_in_segment_in_beat_idx, t1_beats_str, t2_beats_str,
                                                        offset_B_wrt_A_in_beat_idx,
                                                        t1_odf_hwr_per_frame, t2_odf_hwr_per_frame,
                                                        mode=DEFAULT_FE_MODE,
                                                        extended_length_semgents=DEFAULT_EXTENDED_LENGTH_SEGMENTS):

    def extract_sum_onset_of_the_half_beats(t_beats, gamma_hwr_per_frame_idx, beat_idx):
        frame_idx_per_beat_idx = (np.array(t_beats) * float(SAMPLE_RATE) / HOP_SIZE_FFT).astype('int')

        first_half_in_frame_idx = int(frame_idx_per_beat_idx[beat_idx])
        second_half_beat_in_frame_idx = int(
            (frame_idx_per_beat_idx[beat_idx] + frame_idx_per_beat_idx[beat_idx + 1]) / 2)

        first_half_beat_sum_gamma_hwr = np.sum(
            gamma_hwr_per_frame_idx[first_half_in_frame_idx: second_half_beat_in_frame_idx])  # "onset integral"
        second_half_beat_sum_gamma_hwr = np.sum(gamma_hwr_per_frame_idx[
                                                second_half_beat_in_frame_idx: frame_idx_per_beat_idx[
                                                    beat_idx + 1]])  # "onset integral"

        return first_half_beat_sum_gamma_hwr, second_half_beat_sum_gamma_hwr

    features_segment_pair = [[] for odf_method in ODF_METHODS]  # Initialise list for the features of the ODF type.

    # print('DEBUG: LEFT segment of  the segment-pair:')
    # Track A, left segement
    ## Isolated features
    left_segment_isolated_beat_idx = segment_pair_switching_point_in_beat_idx - 1

    left_segment_isolated_first_half_beat_features_dict = dict()  # Initialise
    left_segment_isolated_second_half_beat_features_dict = dict()  # Initialise
    for odf_method_idx, odf_method in enumerate(ODF_METHODS):
        left_segment_isolated_first_half_beat_features_dict[odf_method], \
        left_segment_isolated_second_half_beat_features_dict[odf_method] = extract_sum_onset_of_the_half_beats(
            t1_beats_str, t1_odf_hwr_per_frame[odf_method], left_segment_isolated_beat_idx
        )
        features_segment_pair[odf_method_idx].append(left_segment_isolated_second_half_beat_features_dict[odf_method])
        # print('DEBUG: added feature (value {}) of  isolated second half beat of segment (beat idx {}) for ODF method {}'
        #       .format(left_segment_isolated_second_half_beat_features_dict[odf_method], left_segment_isolated_beat_idx, odf_method))

    ## Contextual features
    if extended_length_semgents is False:
        contextual_beat_idx_start = t1_first_beat_in_segment_in_beat_idx
        contextual_beat_idx_stop = left_segment_isolated_beat_idx - 1 # (included)
    else:
        contextual_beat_idx_start = t1_first_beat_in_segment_in_beat_idx
        contextual_beat_idx_stop = (left_segment_isolated_beat_idx - 1) + EXTENDED_SEGMENT_SIZE_IN_BEATS # (included)
    for odf_method_idx, odf_method in enumerate(ODF_METHODS):
        for contextual_beat_idx in range(contextual_beat_idx_start, contextual_beat_idx_stop+1):
            if mode == ONE_VALUE_DIFFERENCE:
                contextual_first_half_beat_features, contextual_second_half_beat_features = extract_sum_onset_of_the_half_beats(
                    t1_beats_str, t1_odf_hwr_per_frame[odf_method], contextual_beat_idx
                )
                contextual_first_half_beat_difference = contextual_first_half_beat_features - \
                                                        left_segment_isolated_second_half_beat_features_dict[odf_method]
                features_segment_pair[odf_method_idx].append(contextual_first_half_beat_difference)
                # print('DEBUG: added feature (value {}) of contextual first half beat (beat idx {}) for ODF method {}'
                #       .format(contextual_first_half_beat_difference, contextual_beat_idx, odf_method))

                contextual_second_half_beat_difference = contextual_second_half_beat_features - \
                                                         left_segment_isolated_second_half_beat_features_dict[
                                                             odf_method]
                features_segment_pair[odf_method_idx].append(contextual_second_half_beat_difference)
                # print('DEBUG: added feature (value {}) of contextual second half beat (beat idx {}) for ODF method {}'
                #       .format(contextual_second_half_beat_difference, contextual_beat_idx, odf_method))
            elif mode == RUNNING_DIFFERENCE:
                # First difference
                contextual_first_half_beat_features, contextual_second_half_beat_features = extract_sum_onset_of_the_half_beats(
                    t1_beats_str, t1_odf_hwr_per_frame[odf_method], contextual_beat_idx
                )
                contextual_first_difference = contextual_second_half_beat_features - \
                                              contextual_first_half_beat_features
                features_segment_pair[odf_method_idx].append(contextual_first_difference)
                # Second difference
                contextual_first_half_beat_features_next_beat, contextual_second_half_beat_features_next_beat = extract_sum_onset_of_the_half_beats(
                    t1_beats_str, t1_odf_hwr_per_frame[odf_method], contextual_beat_idx + 1
                )
                contextual_second_difference = contextual_first_half_beat_features_next_beat - \
                                                         contextual_second_half_beat_features
                features_segment_pair[odf_method_idx].append(contextual_second_difference)
            else:
                raise Exception('Mode invalid.')
        if extended_length_semgents is False:
            if mode == ONE_VALUE_DIFFERENCE:
                contextual_first_half_beat_difference = left_segment_isolated_first_half_beat_features_dict[odf_method] - \
                                                        left_segment_isolated_second_half_beat_features_dict[odf_method]
                features_segment_pair[odf_method_idx].append(contextual_first_half_beat_difference)
                # print('DEBUG: added feature (value {}) of contextual first half beat (beat idx {}) for ODF method {}'
                #           .format(contextual_first_half_beat_difference, left_segment_isolated_beat_idx, odf_method))
            elif mode == RUNNING_DIFFERENCE:
                contextual_first_difference = left_segment_isolated_second_half_beat_features_dict[odf_method] - \
                                              left_segment_isolated_first_half_beat_features_dict[odf_method]
                features_segment_pair[odf_method_idx].append(contextual_first_difference)
            else:
                raise Exception('Mode invalid.')
        else:
            if mode == RUNNING_DIFFERENCE:
                contextual_first_half_beat_features, contextual_second_half_beat_features = extract_sum_onset_of_the_half_beats(
                    t1_beats_str, t1_odf_hwr_per_frame[odf_method], left_segment_isolated_beat_idx + EXTENDED_SEGMENT_SIZE_IN_BEATS
                )
                contextual_first_difference = contextual_second_half_beat_features - \
                                              contextual_first_half_beat_features
                features_segment_pair[odf_method_idx].append(contextual_first_difference)
            else:
                raise Exception('Mode invalid.')

    # print('DEBUG: t2_last_beat_in_segment_in_beat_idx = {} offset_B_wrt_A_in_beat_idx = {}, '
    #       .format(t2_last_beat_in_segment_in_beat_idx, offset_B_wrt_A_in_beat_idx))

    # print('DEBUG: RIGHT segment of  the segment-pair:')
    # Track B, right segment
    ## Isolated features
    right_segment_isolated_beat_idx = segment_pair_switching_point_in_beat_idx - offset_B_wrt_A_in_beat_idx
    right_segment_isolated_first_half_beat_features_dict = dict()  # Initialise
    right_segment_isolated_second_half_beat_features_dict = dict()  # Initialise
    for odf_method_idx, odf_method in enumerate(ODF_METHODS):
        right_segment_isolated_first_half_beat_features_dict[odf_method], \
        right_segment_isolated_second_half_beat_features_dict[odf_method] = extract_sum_onset_of_the_half_beats(
            t2_beats_str, t2_odf_hwr_per_frame[odf_method], right_segment_isolated_beat_idx
        )
        features_segment_pair[odf_method_idx].append(right_segment_isolated_first_half_beat_features_dict[odf_method])
        # print('DEBUG: added feature (value {}) of  isolated first half beat of segment (beat idx {}) for ODF method {}'
        #       .format(right_segment_isolated_first_half_beat_features_dict[odf_method], right_segment_isolated_beat_idx, odf_method))
    ## Contextual features
    for odf_method_idx, odf_method in enumerate(ODF_METHODS):
        if extended_length_semgents is False:
            contextual_second_half_beat_difference = right_segment_isolated_second_half_beat_features_dict[odf_method] - \
                                                     right_segment_isolated_first_half_beat_features_dict[odf_method]
            features_segment_pair[odf_method_idx].append(contextual_second_half_beat_difference)
            # print('DEBUG: added feature (value {}) of contextual second half beat (beat idx {}) for ODF method {}'
            #           .format(contextual_second_half_beat_difference, right_segment_isolated_beat_idx, odf_method))
            # Note: this is idem for RUNNING_DIFFERENCE mode
        else:
            if mode == RUNNING_DIFFERENCE:
                contextual_first_half_beat_features, contextual_second_half_beat_features = extract_sum_onset_of_the_half_beats(
                    t2_beats_str, t2_odf_hwr_per_frame[odf_method], right_segment_isolated_beat_idx - EXTENDED_SEGMENT_SIZE_IN_BEATS
                )
                contextual_first_difference = contextual_second_half_beat_features - \
                                              contextual_first_half_beat_features
                features_segment_pair[odf_method_idx].append(contextual_first_difference)
            else:
                raise Exception('Mode invalid.')


        if extended_length_semgents is False:
            contextual_beat_idx_start = right_segment_isolated_beat_idx + 1
            contextual_beat_idx_stop = t2_last_beat_in_segment_in_beat_idx - offset_B_wrt_A_in_beat_idx  # (included)
        else:
            contextual_beat_idx_start = (right_segment_isolated_beat_idx + 1) - EXTENDED_SEGMENT_SIZE_IN_BEATS
            contextual_beat_idx_stop = t2_last_beat_in_segment_in_beat_idx - offset_B_wrt_A_in_beat_idx  # (included)
        for contextual_beat_idx in range(contextual_beat_idx_start, contextual_beat_idx_stop+1):
            if mode == ONE_VALUE_DIFFERENCE:
                contextual_first_half_beat_features, contextual_second_half_beat_features = extract_sum_onset_of_the_half_beats(
                    t2_beats_str, t2_odf_hwr_per_frame[odf_method], contextual_beat_idx
                )
                contextual_first_half_beat_difference = contextual_first_half_beat_features - \
                                                        right_segment_isolated_first_half_beat_features_dict[odf_method]
                features_segment_pair[odf_method_idx].append(contextual_first_half_beat_difference)
                # print('DEBUG: added feature (value {}) of contextual first half beat (beat idx {}) for ODF method {}'
                #       .format(contextual_first_half_beat_difference, contextual_beat_idx, odf_method))

                contextual_second_half_beat_difference = contextual_second_half_beat_features - \
                                                         right_segment_isolated_first_half_beat_features_dict[
                                                             odf_method]
                features_segment_pair[odf_method_idx].append(contextual_second_half_beat_difference)
                # print('DEBUG: added feature (value {}) of contextual second half beat (beat idx {}) for ODF method {}'
                #       .format(contextual_second_half_beat_difference, contextual_beat_idx, odf_method))
            elif mode == RUNNING_DIFFERENCE:
                # Second difference of the previous beat
                contextual_first_half_beat_features, contextual_second_half_beat_features = extract_sum_onset_of_the_half_beats(
                    t2_beats_str, t2_odf_hwr_per_frame[odf_method], contextual_beat_idx
                )
                contextual_first_half_beat_features_prev_beat, contextual_second_half_beat_features_prev_beat = extract_sum_onset_of_the_half_beats(
                    t2_beats_str, t2_odf_hwr_per_frame[odf_method], contextual_beat_idx - 1
                )
                contextual_second_difference_prev_beat = contextual_first_half_beat_features - \
                                                         contextual_second_half_beat_features_prev_beat
                features_segment_pair[odf_method_idx].append(contextual_second_difference_prev_beat)
                # First difference (of the current beat)
                contextual_first_difference = contextual_second_half_beat_features - \
                                              contextual_first_half_beat_features
                features_segment_pair[odf_method_idx].append(contextual_first_difference)
            else:
                raise Exception('Mode invalid.')

    return features_segment_pair


'''
Note: All metrics are wrt T1
Note: these features are extracted for switching point (in beat idx) (= segment_pair_central_point_in_beat_idx + 1)
Note: Returns None if the segment-pair is out of bounds.
The default values in the kwargs are usually used in the live djing tool.
'''
def extract_all_features_of_segment_pair(segment_pair_switching_point_in_beat_idx,
                                         t1_beats_str, t2_beats_str,
                                         offset_B_wrt_A_in_beat_idx,
                                         t1_cf_ML, t2_cf_ML,
                                         mode=DEFAULT_FE_MODE,
                                         extended_length_semgents=DEFAULT_EXTENDED_LENGTH_SEGMENTS):
    t1_first_beat_in_segment_in_beat_idx = segment_pair_switching_point_in_beat_idx - SEGMENT_SIZE_IN_BEATS
    t2_last_beat_in_segment_in_beat_idx = segment_pair_switching_point_in_beat_idx + SEGMENT_SIZE_IN_BEATS - 1  # (included)
    # verify that not out of bounds for T1 or T2
    if not (t1_first_beat_in_segment_in_beat_idx >= 0 and segment_pair_switching_point_in_beat_idx - 1 < len(
            t1_beats_str)):
        print('ERROR: Left (track A) segment of the pair is out of bounds')
        return None
    if not (t2_last_beat_in_segment_in_beat_idx - offset_B_wrt_A_in_beat_idx < len(t2_beats_str)
            and segment_pair_switching_point_in_beat_idx - offset_B_wrt_A_in_beat_idx >= 0):
        print('ERROR: Right (track B) segment of the pair is out of bounds\n'
                        'not (t2_last_beat_in_segment_in_beat_idx - offset_B_wrt_A_in_beat_idx < len(t2_beats_str) '
                        'and segment_pair_switching_point_in_beat_idx - offset_B_wrt_A_in_beat_idx >= 0)\n with:\n'
                        't2_last_beat_in_segment_in_beat_idx = {}\n'
                        'offset_B_wrt_A_in_beat_idx = {}\n'
                        'len(t2_beats_str) = {}\n'
                        'segment_pair_switching_point_in_beat_idx = {}\n'
                        ''.format(t2_last_beat_in_segment_in_beat_idx, offset_B_wrt_A_in_beat_idx,
                                  len(t2_beats_str), segment_pair_switching_point_in_beat_idx))
        return None
    # Feature extraction

    e_in_melbands_for_segment_pair = extract_feature_e_in_melbands_for_segment_pair(
        t1_first_beat_in_segment_in_beat_idx, segment_pair_switching_point_in_beat_idx,
        t2_last_beat_in_segment_in_beat_idx,
        offset_B_wrt_A_in_beat_idx,
        t1_cf_ML['mfcc_per_beat'], t2_cf_ML['mfcc_per_beat'],
        mode=mode,
        extended_length_semgents=extended_length_semgents
    )
    # print 'DEBUG: len(features datapoint) = {}'.format(len(e_in_melbands_for_segment_pair), e_in_melbands_for_segment_pair)

    loudness_for_segment_pair = extract_feature_loudness_for_segment_pair(
        t1_first_beat_in_segment_in_beat_idx, segment_pair_switching_point_in_beat_idx,
        t2_last_beat_in_segment_in_beat_idx, offset_B_wrt_A_in_beat_idx,
        t1_cf_ML['loudness_per_beat'], t2_cf_ML['loudness_per_beat'],
        mode=mode,
        extended_length_semgents=extended_length_semgents
    )

    sum_onset_function_for_segment_pair = extract_feature_sum_onset_function_for_segment_pair(
        t1_first_beat_in_segment_in_beat_idx, segment_pair_switching_point_in_beat_idx,
        t2_last_beat_in_segment_in_beat_idx, t1_beats_str, t2_beats_str,
        offset_B_wrt_A_in_beat_idx,
        t1_cf_ML['odf_hwr_per_frame'], t2_cf_ML['odf_hwr_per_frame'],
        mode=mode,
        extended_length_semgents=extended_length_semgents
    )

    features_of_data_point = [
        e_in_melbands_for_segment_pair,
        sum_onset_function_for_segment_pair,
        loudness_for_segment_pair
    ]
    return features_of_data_point

# Auxiliary methods

'''
Prepare the features for the ML input
features_of_one_data_point: use if X is needed of one datapoint (used for prediction)
'''


def to_X_and_target(features_of_datapoints, target_of_datapoints, features_of_one_data_point = False):
    amount_feature_types = np.array(features_of_datapoints[0]).shape[0]

    _X_all = []
    for features_of_datapoint in features_of_datapoints:
        features_of_datapoint_flattened = np.array([])
        for features_type_idx in range(amount_feature_types):
            features_of_datapoint_flattened = np.concatenate(
                (features_of_datapoint_flattened, np.array(features_of_datapoint[features_type_idx]).flatten()))
        if features_of_one_data_point:
            return features_of_datapoint_flattened
        _X_all.append(features_of_datapoint_flattened)
    X_all = np.array(_X_all)
    target_all = np.array(target_of_datapoints)
    return X_all, target_all

if __name__ == '__main__':
    pass